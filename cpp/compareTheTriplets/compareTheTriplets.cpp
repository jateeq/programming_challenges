#include <iostream>
#include <vector>

using namespace std;

const int NUM_SCORES_PER_PERSON = 3;

vector<int> getScoresFromCmdLine( int num_scores ){
    vector<int> scores( num_scores );

    for(int i=0; i<NUM_SCORES_PER_PERSON; i++){
        cin >> scores[i];
    }

    return scores;
}

class Player{
    public:
        vector<int> scores;
        int points;
};

int main(){
    vector<int> scoresA( NUM_SCORES_PER_PERSON );
    vector<int> scoresB( NUM_SCORES_PER_PERSON );

    cout << "Enter " << NUM_SCORES_PER_PERSON << " scores for player A" << endl;
    scoresA = getScoresFromCmdLine( NUM_SCORES_PER_PERSON );

    cout << "Enter " << NUM_SCORES_PER_PERSON << " scores for player B" << endl;
    scoresB = getScoresFromCmdLine( NUM_SCORES_PER_PERSON );

    Player playerA, playerB;

    playerA.scores = scoresA;
    playerB.scores = scoresB;

    for( int i=0; i<NUM_SCORES_PER_PERSON; i++ ){
        if( playerA.scores[i] != playerB.scores[i] ){
            if( playerA.scores[i] > playerB.scores[i] ){
                playerA.points += 1;
            }
            else{
                playerB.points += 1;
            }
        }
    }

    cout << playerA.points << " " << playerB.points << endl;
}
