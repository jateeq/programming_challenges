lst = [1, 4, 6, 3, 4, 5, 64, 1,10, 32]

def sum_recursive(lst):
    if len(lst) == 1:
        return lst[0]
    else:
        return sum(lst[0:len(lst)-1]) + lst[-1]

def sum_for(lst):
    total = 0
    for n in lst:
        total += n
    return total

def sum_while(lst):
    loop_counter = 0
    total = 0
    while( loop_counter < len(lst) ):
        total += lst[loop_counter]
        loop_counter += 1
    return total

if __name__ == "__main__":
    print "Actual sum: " + str(sum(lst))
    print "Recursive sum: " + str(sum_recursive(lst))
    print "For sum: " + str(sum_for(lst))
    print "While sum: " + str(sum_while(lst))
