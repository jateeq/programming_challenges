#include <cmath>
#include "veryBigSum.h"
#include "gtest/gtest.h"

const int num_values = 10;

TEST( isSufficientMemory, testWorstCaseSum ){
    long long int value = pow( 10, 11 );
    vector<long long int> list( num_values, value );
    EXPECT_EQ( calculateVeryLargeSum( list ), num_values * value ); 
}
