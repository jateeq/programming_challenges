#include "utils.h"
#include <iostream>
#include <algorithm>

vector<char> getUniqueChars( string s ){
    vector<char> unique_chars;
    for( int i=0; i<s.length(); i++ ){
        if( find( unique_chars.begin(), unique_chars.end(), s[i] ) == unique_chars.end() ){
            unique_chars.push_back(s[i]); 
        }
    }
    return unique_chars;
}

string deleteCharFromString( string s, string c ){
    size_t pos = s.find(c);
    while(  pos != string::npos ){
        s.erase( pos, 1);
        pos = s.find(c);
    }
    return s;
}

bool hasOnlyTwoUniqueCharacters( string s ){
    vector<int> controlArray(256);    
    for(int i=0; i<s.length(); i++){
        int val = s[i];
        if( !controlArray[val] ){
            controlArray[val] = 1;
        }
    }
    return arraySum(controlArray) == 2; 
}

bool hasAlternatingChars( string s ){
    if( s.length() == 1 ){
        return false;
    }
    else if( s.length() == 2 ){
        return (s[0] != s[1]);
    }
    else{
        for( int i=0; i<s.length()-2; i++){
            if( s[i] == s[i+1] || s[i] != s[i+2] ){
                return false;
            }
        }
        return true;
    }
}

int arraySum( vector<int> A){
    int sum = 0;
    for(int i=0; i<A.size(); i++){
        sum += A[i];
    }
    return sum;
}
