#ifndef UTILS
#define UTILS

#include <string>
#include <vector>

using namespace std;

string get_sentence_or_sequence( string s, int min, int max);
string get_sentence_or_sequence_from_vector( vector<string> list_of_strings, int min, int max);
vector<string> tokenize_sentence( string line );
#endif
