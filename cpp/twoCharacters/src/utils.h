#ifndef UTILS
#define UTILS

#include <vector>
#include <string>

using namespace std;

vector<char> getUniqueChars( string s );
string deleteCharFromString( string s, string c );
bool hasAlternatingChars( string s );
bool hasOnlyTwoUniqueCharacters( string s );
int arraySum( vector<int> A);


#endif
