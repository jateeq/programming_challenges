import pdb

def generate_fibonacci_recursive(n):
    if n <= 2:
        return [0, 1]
    else:
        lst = generate_fibonacci_recursive(n-1)
        lst.append(lst[-1] + lst[-2])
        return lst


if __name__ == "__main__":
    print generate_fibonacci_recursive(20)

