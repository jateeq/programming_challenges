#ifndef PROCESS_STRING_H
#define PROCESS_STRING_H

#include <string>
#include <vector>
#include <ctime>
#include <iostream>
#include <algorithm>
#include "node.h"
#include "utils.h"

using namespace std;

int get_max_two_char_length( string s );
int get_max_two_char_length_optimized( string s );
Node * buildTree( string s );
int get_max_length( Node * root );

#endif
