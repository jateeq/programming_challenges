import math
import pdb

def construct_largest_number(lst):
    # find the largest number in range [0-9]
    i = 0
#    target_lst = []
#    while i < len(lst):
#        lst = sort_list(lst)
#        target_lst.append(next_num_to_add)
#
#    return list_to_number(target_list)
        # if the number > 9, use the left most number for comparison
    # append this number a list, and remove it from the input list
    # repeat as long as there are still numbers in the list

def swap_list(lst, n1, n2):
    temp = lst[n1]
    lst[n1] = lst[n2]
    lst[n2] = temp
    return lst

def sort_list(lst):
    if len(lst) == 1:
        return lst
    else:
        sorted_list = sort_list(lst[:-2])
        right = lst[:-1]
        left = sorted_list[:-1]
        if left > right:
            temp = left
            left = right
            right = temp
        sorted_list[:-2] = left
        sorted_list[:-1] = right
        return sorted_list

def list_to_number(lst):
    if len(lst) == 1:
        return lst[0]
    else:
        return list_to_number(lst[1:]) + (lst[0] * math.pow(10,len(lst)-1))

if __name__ == "__main__":
    print sort_list([2,1,3])
