#include "gtest/gtest.h"
#include "process_string.h"
#include <ctime>

TEST( TwoCharRepresentation, testMaxLengthSingleChar ){
    string s = "b";
    int max_length = get_max_two_char_length( s );
    EXPECT_EQ( max_length, 0 );
}

TEST( TwoCharRepresentation, testMaxLengthDoubleChar ){
    string s = "be";
    int max_length = get_max_two_char_length( s );
    EXPECT_EQ( max_length, 2 );
}


TEST( TwoCharRepresentation, testMaxLengthMultiChar ){
    string s = "beabeefeab";
    int max_length = get_max_two_char_length( s );
    EXPECT_EQ( max_length, 5 );

    clock_t begin = clock();
    s = "beabeefeabadfakdfjadfjfoiwekjfafjdkjriafdkjfk";
    //max_length = get_max_two_char_length( s );
    //EXPECT_EQ( max_length, 5 );
    clock_t end = clock();
    cout << "Unoptimized solution took  " << double (end - begin)/CLOCKS_PER_SEC << " seconds" << endl;

}

TEST( TwoCharRepresentation, testMaxLengthMultiCharOptimized ){
    string s = "beabeefeab";
    int max_length = get_max_two_char_length_optimized( s );
    EXPECT_EQ( max_length, 5 );

    clock_t begin = clock();
    s = "bbeabeefeabbeabeefeab";
    max_length = get_max_two_char_length_optimized( s );
    EXPECT_EQ( max_length, 5 );
    clock_t end = clock();
    cout << "Optimized solution took " << double (end - begin)/CLOCKS_PER_SEC << " seconds" << endl;
}
