#include <iostream>
#include <vector>

using namespace std;

vector<int> getVectorInputFromCommandLine(int len_inputs){
   vector<int> inputs(len_inputs);

    for(int i=0; i<len_inputs; i++)
        cin >> inputs[i];

   return inputs;
}

int calculateVectorSum( vector<int> inputs ){
    int sum=0;
    
    for( int i=0; i<inputs.size(); i++)
        sum += inputs[i];

    return sum;
}

int main(){
    cout << "This program calculates the sum of an input array" << endl << endl;

    int len_inputs;

    cout << "Please enter the number of elements in the array:" << endl;
    cin >> len_inputs;

    vector<int> inputs = getVectorInputFromCommandLine(len_inputs);

    cout << "The sum is: " << calculateVectorSum(inputs) << endl;
}
