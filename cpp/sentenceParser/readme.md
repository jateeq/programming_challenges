## Problem Statement 
You are free to use any reference resources (i.e. internet, books, etc.).  I just ask that you don’t try and search for the solution to this problem.  Proper software design is more important than a working solution, consider carefully how you architect the solution, create functions, define variables and their scope and overall readability of the software

Write a program to find all the sentences, or consecutive sequence of sentences, in a text file where:  min <= length <= max.
Assume that a sentence ends in a period, question mark, or exclamation point.  There is no fixed limit on the length of a line in the file or a practical max number of lines in the file.

Count all blanks and punctuation.  Include the trailing space after each sentence, but assume there is ever only one blank between sentences.
(All EOL/null characters should be converted to blanks as well).

The output of the program should be sent to another text file and be sorted in ascending order based on the ASCII value of the first character of each line in the output.  

Precondition: Min and Max will be positive integers less than 1000, and Min <= Max.

The name of the input and output text file is to be provided as a command line argument (not read from Standard Input or written to Standard Output).

Example Usage:  program.exe <min> <max> <input file> <output file>

For example, given this input text:
Black is white. Day is night. Understanding is ignorance.
Truth is fiction. Safety is danger.

If min = max = 16 then the output is 
    Black is white.
If min = max = 18 then the output is 
  Safety is danger.
    Truth is fiction.
If min = 30 and max = 37 then the output is

  Black is white. Day is night. 
    Truth is fiction. Safety is danger.
because the two sentences are consecutive sentences with the desired length.

EndFragment


## Setup
Type 'make' to compile all targets. The output is generated in the build/directory. To run the
main file, run 'build/main'.

## Design Decisions
- Had to add the -std=c++11 cmd line option to use the new(er) standard of initiaizeing vectors. 
- Assumed lines are separated by EOL characters. Multiple stentences can be in a line but the algorithm stop looking once it hits the end of the line.
- 

## Testing
I used Google's GUnit for unit testing (set up instructions are here: 
https://github.com/google/googletest/blob/master/googletest/docs/Primer.md). 

To run the tests, run build/*_unittest.
