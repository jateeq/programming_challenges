#include "utils.h"
#include <iostream>

vector<string> tokenize_sentence( string line ){
    int start = 0;
    vector<string> list_of_strings;
    
    int end = line.find_first_of( ".!?", start ); 
    while( end != string::npos ){
        string target = line.substr( start, end+1 );
        cout << "string is: " << target << endl;
        list_of_strings.push_back( target );
        start = end + 1;
        end = line.find_first_of(".!?", start);
    }
    return list_of_strings;
}

string get_sentence_or_sequence_from_vector( vector<string> list_of_strings, int min, int max){
    string result = "";

    for( int i=0; i<list_of_strings.size(); i++){
        string cur_str = list_of_strings[i];
        int sentence_length = cur_str.length();
        if( sentence_length >= min && sentence_length <= max){
            result.append( cur_str ); 
            min = 0;
            max -= sentence_length;
        }
    }
    return result;
}

string get_sentence_or_sequence( string s, int min, int max){
    vector<string> list_of_strings = tokenize_sentence( s );
    return get_sentence_or_sequence_from_vector( list_of_strings, min, max );
}
