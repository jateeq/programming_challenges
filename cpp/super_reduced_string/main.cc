#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    string s;
    cin >> s;
    int len = s.length();
    
    int index = 1;
    while( index < s.length() ){
        if( s[index] == s[index - 1]){
            s.erase( index-1, 2 );
            index = 1;
        }
        else{
            index += 1;
        }
    }
    
    if( s.length() > 0){
        cout << s << endl;
    }
    else{
        cout << "Empty String";
    }
    
    return 0;
}
