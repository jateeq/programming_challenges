#include "elasticRope.h"
#include <iostream>
#include <math.h>
#include <algorithm>

using namespace std;


float findMaxDistanceBetweenPoints( vector<Point> vertices, int start_vertex, int end_vertex){
    float clockwise_distance, counterclockwise_distance;

    clockwise_distance = findMaxDistanceBetweenPointsCCW( vertices, start_vertex,
            end_vertex);
    counterclockwise_distance = findMaxDistanceBetweenPointsCCW( vertices,
            end_vertex, start_vertex );
    
    return max(clockwise_distance, counterclockwise_distance);
}


float findMaxDistanceBetweenPointsCCW( vector<Point> vertices, int start_vertex,
        int end_vertex){
    
    vector<Point> traversal_list = buildPathBetweenPointsCCW( vertices, start_vertex, end_vertex );
       
    float distance = findDistanceOfPath( traversal_list );
    return distance;
}

vector<Point> buildPathBetweenPointsCCW( vector<Point> vertices, int start_vertex, int end_vertex)
{
    int cur_vertex = start_vertex;
    vector<Point> traversal_list; 
    traversal_list.push_back( vertices[cur_vertex] );

    //for( int i=start_vertex+1; i!=end_vertex; i++ ){
    /*for( int i=start_vertex; i!=end_vertex; i++ ){
        if( i==vertices.size() ){
            i = 0;
        }

        cur_vertex = getIndexOfNextVertex( vertices, cur_vertex, end_vertex );
        traversal_list.push_back( vertices[cur_vertex] );

        if( i==end_vertex){
            break;
        }
    }*/

    while( cur_vertex != end_vertex ){
        cur_vertex = getIndexOfNextVertex( vertices, cur_vertex, end_vertex );
        traversal_list.push_back( vertices[cur_vertex] );
    }

    return traversal_list;
}

int getIndexOfNextVertex( vector<Point> vertices, int start_vertex, int end_vertex ){
    float min_angle = 360;
    int best_vertex;

    for(int i=start_vertex+1; i<vertices.size()+1; i++){
        if( i == vertices.size() ){
           i = 0; 
        }

        float angle = getAngleBetweenPoints( vertices[ start_vertex ], vertices[ i ]);

        if( angle <= min_angle ){
            min_angle = angle;
            best_vertex = i;
        }
        cout << "i: " << i << ", Angle: " << angle << endl;

        if( i == end_vertex ){
            break;
        }
    }
    return best_vertex;
}


float getAngleBetweenPoints( Point a, Point b){
    float result = atan2( b.y - a.y,  b.x - a.x ) * 180 / M_PI;

    if( result < 0 ){
        result = result + 360;
    }

    return result;
}


float findDistanceOfPath( vector<Point> vertices ){
    float distance = 0;

    for( int i=1; i<vertices.size(); i++){
        float x = vertices[i].x - vertices[i-1].x;
        float y = vertices[i].y - vertices[i-1].y;
        distance += sqrt(x*x + y*y);
    }

    return distance;
}


void printVectorOfPoints( vector<Point> points ){
    for( int i=0; i<points.size(); i++){
        cout << (points[i]).x << " " << (points[i].y) << endl;
    }
}

