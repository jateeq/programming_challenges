#include <iostream>
#include "veryBigSum.h"

long long int calculateVeryLargeSum( vector<long long int> list ){
    long long int sum = 0;
    for( int i=0; i<list.size(); i++){
        sum += list[i];
    }
    return sum;
}
