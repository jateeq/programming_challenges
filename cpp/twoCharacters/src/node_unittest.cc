#include "gtest/gtest.h"
#include "node.h"
#include <iostream>

TEST( NodeAccess, CheckGettingStringFromNode ){
    string s = "test";
    Node n( s );
    EXPECT_EQ( n.getNodeValue(), s );
}

TEST( NodeAccess, CheckSettingNodeString ){
    string s = "test";
    Node n;
    n.setNodeValue( s );
    EXPECT_EQ( n.getNodeValue(), s );
}

TEST( NodeRelatives, CheckAddingNodeParent ){
    Node* n = new Node( "child" );
    Node* p = new Node( "parent" );
    n->setParent( p );
    EXPECT_EQ( (n->getParent())->getNodeValue(), "parent" );
    delete n;
    delete p;
}

TEST( NodeRelatives, CheckDeletingNodeParent ){
    Node* n = new Node( "child" );
    Node* p = new Node( "parent" );
    n->setParent( p );
    EXPECT_EQ( (n->getParent())->getNodeValue(), "parent" );
    n->deleteParent();
    EXPECT_EQ( n->getParent(), nullptr );
    //delete n;
}

TEST( NodeRelatives, CheckAddingChildren){
    Node* n = new Node( "parent" );
    Node* c1 = new Node( "child1" );
    Node* c2 = new Node( "child2" );
    n->addChild( c1 );
    n->addChild( c2 );
    vector<Node*> childNodes = n->getChildren();
    EXPECT_EQ( childNodes[0]->getNodeValue(), "child1" );
    EXPECT_EQ( childNodes[1]->getNodeValue(), "child2" );
    //delete n;
}
/*
TEST( NodeRelatives, CheckRemovingChildren){
    Node* n = new Node( "parent" );
    Node* c1 = new Node( "child1" );
    Node* c2 = new Node( "child2" );
    n->addChild( c1 );
    n->addChild( c2 );
    n->removeChild( 2 );
    vector<Node*> children = n->getChildren();
    EXPECT_EQ( children.size(), 2 );
    n->removeChild( 1 );
    EXPECT_EQ( children.size(), 1 );
    EXPECT_EQ( children[1], nullptr );
    EXPECT_EQ( children[0]->getNodeValue(), "child1" );
    delete n;
}*/
