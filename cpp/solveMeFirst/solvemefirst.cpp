// Hacker rank challenge number 
#include <iostream>

using namespace std;

int calculate_sum( int a, int b ) {
    return a+b;
}

int main() {
    int a, b;
    int sum;

    cout << "This program calcualtes the sum of two input numbers" << endl;

    cout << "Enter the first number" << endl;
    cin >> a;
    cout << "Enter the second number" << endl;
    cin >> b;

    sum = calculate_sum( a,b );

    cout << "The sum of " << a << " and " << b << " is: " << sum << endl;
}
