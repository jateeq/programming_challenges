#include "gtest/gtest.h"
#include "utils.h"

TEST( StringValidityChecking, CheckTwoUniqueCharactersMultiCase ){
    string s = "beabeefeab"; 
    EXPECT_EQ( hasOnlyTwoUniqueCharacters(s), false ); 
    s = "babababab";
    EXPECT_EQ( hasOnlyTwoUniqueCharacters(s), true );
}

TEST( StringValidityChecking, CheckTwoUniqueCharactersSingleCharCase ){
    string s = "b";
    EXPECT_EQ( hasOnlyTwoUniqueCharacters(s), false );
}

TEST( StringValidityChecking, CheckTwoUniqueCharactersTwoCharCase ){
    string s = "ba";
    EXPECT_EQ( hasOnlyTwoUniqueCharacters(s), true ); 
    s = "bb";
    EXPECT_EQ( hasOnlyTwoUniqueCharacters(s), false );
}

TEST( StringValidityChecking, CheckIsAlternating ){
    string s = "beabeefeab"; 
    EXPECT_EQ( hasAlternatingChars(s), false );
    s = "bababab";
    EXPECT_EQ( hasAlternatingChars(s), true );
    }

TEST( StringValidityChecking, CheckIsAlternatingSingleCase ){
    string s = "b";
    EXPECT_EQ( hasAlternatingChars(s), false );
}

TEST( StringValidityChecking, CheckIsAlternatingTwoCharCase ){
    string s = "bb";
    EXPECT_EQ( hasAlternatingChars(s), false );
    s = "ba";
    EXPECT_EQ( hasAlternatingChars(s), true );
}

TEST( StringManipulation, DeleteAllOccurrencesOfSpecifiedCharMultiCase ){
    string s = "beabeefeab"; 
    string c = "a";
    string d = "bebeefeb";
    EXPECT_EQ( deleteCharFromString( s, c),  d);
    c = "e";
    d = "babfab";
    EXPECT_EQ( deleteCharFromString( s, c), d );
}

TEST( StringManipulation, DeleteAllOccurrencesOfSpecifiedCharSingleCase ){
    string s = "b";
    string c = "e";
    EXPECT_EQ( deleteCharFromString( s, c), s );
    c = "";
    EXPECT_EQ( deleteCharFromString( s, s), c );
}

TEST( StringManipulation, GetUniqueCharactersInStringMultiCharCase){
    string s = "abcdef";
    vector<char> result = {'a', 'b', 'c', 'd', 'e', 'f'};
    EXPECT_EQ( getUniqueChars(s), result );
}

