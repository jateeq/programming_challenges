import numpy as np
import pdb

lst1 = [1, 2, 3, 4, 5, 6]
lst2 = ['b', 'c']

def get_max_len(big_list):
    n = 0
    for lst in big_list:
        if len(lst) > n:
            n = len(lst)
    return n

def combine_lists(lst1, lst2):
    big_list = [lst1, lst2]
    n = get_max_len(big_list)

    result = []
    j = 0
    num_lists = 2
    while j < n:
        i = 0
        while i < num_lists:
            if j < len(big_list[i]):
                result.append(big_list[i][j])
            i += 1
        j += 1
    return result



if __name__ == "__main__":
    print combine_lists(lst1, lst2)
