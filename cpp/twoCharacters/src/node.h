#ifndef NODE_H
#define NODE_H

#include <string>
#include <vector>
// A node in our tree contains a string, and flags to indicate whether it has only two 
// unique characters, and whether the characters are alternating
// It also contains a pointer to a parent node, and pointers to children nodes. 
//

using namespace std;

class Node{
private:
    bool isDistinct;
    bool isAlternate;
    string nodeValue;
    Node* parentNode;
    vector<Node*> childNodes;

public:
    Node();
    Node( string s );
    //~Node( );
    string getNodeValue() { return this->nodeValue; }
    void setNodeValue( string s ) { this->nodeValue = s; }
    vector<Node*> getChildren() { return this->childNodes; }
    void addChild( Node * childNode ) { this->childNodes.push_back( childNode ); }
    //void removeChild( int index );
    Node* getParent() { return this->parentNode; }
    void setParent( Node * parentNode ) { this->parentNode = parentNode; }
    void deleteParent();
    bool getIsDistinct() { return this->isDistinct; }
    void setIsDistinct( bool value ) { this->isDistinct = value; }
    bool getIsAlternate() { return this->isAlternate; }
    void setIsAlternate( bool value ) { this->isAlternate = value; }
};

#endif
