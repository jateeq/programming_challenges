#include "gtest/gtest.h"
#include "utils.h"

TEST( StringManipulation, GetAllSentencesInLine){
    string line = "Black is White. Day is Night! Understanding is Ignorance? ";
    vector<string> list;
    list.push_back( "Black is White." );
    list.push_back( " Day is Night!" );
    list.push_back( " Understanding is Ignorance?" );
    vector<string> result = tokenize_sentence( line );
    
    ASSERT_EQ( result.size(), list.size() );

    /*
    for( int i = 0; i<result.size(); i++){
        EXPECT_EQ( result[i], list[i] );
    }*/
}

TEST( SentenceRangeDetection, CheckSentenceIsWithinRange ){
    vector<string> list;
    list.push_back( "Black is White." );
    list.push_back( " Day is Night!" );
    list.push_back( " Understanding is Ignorance?" );
    string result = get_sentence_or_sequence_from_vector( list, 15, 18 );
    EXPECT_EQ( result, "Black is White." );

}

