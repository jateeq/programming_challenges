#include "process_string.h"

int get_max_two_char_length( string s ){
    clock_t begin = clock();
    Node* tree = buildTree( s );
    clock_t end= clock();
    cout << "Tree building took " << double (end - begin)/CLOCKS_PER_SEC << " seconds" << endl;
    begin = clock();
    int result = get_max_length( tree );
    end= clock();
    cout << "Tree traversal took " << double (end - begin)/CLOCKS_PER_SEC << " seconds" << endl;
    return result;
}


int get_max_two_char_length_optimized( string s ){
    vector<char> uniqueChars = getUniqueChars( s );
    int num_unique_chars = uniqueChars.size();
    sort( uniqueChars.begin(), uniqueChars.end() );
    
    vector<int> lengths;
    string target;
    string cur_char; 
    for(int i=0; i<num_unique_chars; i++){
        target = s;
        for(int j=i; j<num_unique_chars; j++){
            cur_char = uniqueChars[j]; 
            target = deleteCharFromString( target, cur_char );  
            if( hasOnlyTwoUniqueCharacters( target ) ){
                if( hasAlternatingChars( target ) ){
                    lengths.push_back( target.size() );
                }
                break;
            }
        }
    }

    int result;
    if( lengths.size() > 1 ){
        vector<int>::iterator lengths_iterator = max_element( lengths.begin(), lengths.end() );
        int max_index = distance( lengths.begin(), lengths_iterator);
        int max_length = lengths[ max_index ];
        result = max_length;
    }
    else
    {
        result = 0;
    }
   
    return result;
} 

Node * buildTree( string s ){
    Node * root = new Node( s );
    root->setIsDistinct( hasOnlyTwoUniqueCharacters( s ) ); 
    root->setIsAlternate( hasAlternatingChars( s ) );

    if( !root->getIsDistinct() && !root->getIsAlternate() ){ 
        vector<char> uniqueChars = getUniqueChars( s );
        for(int i = 0; i<uniqueChars.size(); i++){
            string permutation = deleteCharFromString( s, string( 1, uniqueChars[i] ));
            Node * child = buildTree( permutation );
            root->addChild( child );
        }
    }

    return root;
}

int get_max_length( Node * root ){
    string s = root->getNodeValue();
    int result = 0;
    if( root->getIsDistinct() && root->getIsAlternate() ){
        result = s.length();
    }
    else{
        vector<Node*> children = root->getChildren();
        for(int i=0; i<children.size(); i++){
           int child_result= get_max_length( children[i] ); 
           if( child_result > result ){
                result = child_result;
           }
        }
    }
    return result;
}
