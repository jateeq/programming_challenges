#include <iostream>
#include "elasticRope.h"

// Note that the points in the polygon are provided in counter clockwise order

int main(){
    short num_vertices, start_vertex, end_vertex;

    //cin >> num_vertices >> start_vertex >> end_vertex;

    //cout << "num vertices: " << num_vertices << endl;
    //cout << "start_vertex: " << start_vertex << endl;
    //cout << "end vertex: " << end_vertex << endl;

    num_vertices = 6;
    start_vertex = 3;
    end_vertex = 0;

    vector<Point> vertices( num_vertices ); 

    /*for( int i=0; i<num_vertices; i++){
        int x, y; 
        cin >> x >> y;
        vertices[i] = Point(x, y);
    }*/

    
    vertices[0] = Point( 167, 84 ); 
    vertices[1] = Point( 421, 84 ); 
    vertices[2] = Point( 283, 192 ); 
    vertices[3] = Point( 433, 298 );
    vertices[4] = Point( 164, 275 );
    vertices[5] = Point( 320, 133 );

    //cout << getAngleBetweenPoints( vertices[4], vertices[5] );
    //int bestvertex = getIndexOfNextVertex( vertices, 5, 1 );
    //cout << "berst vertex: " << bestvertex << endl;
    //
    /*start_vertex = 4;
    end_vertex = 3;
    vector<Point> list = buildPathBetweenPointsCCW(vertices, start_vertex, end_vertex);
    printVectorOfPoints(list);*/

    float max_distance = findMaxDistanceBetweenPoints( vertices, start_vertex, end_vertex);
    cout << "Answer: " << max_distance << endl;
}


