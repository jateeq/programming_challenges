#include "point.h"
#include <vector>

using namespace std;

void printVectorOfPoints( vector<Point> points );
float findMaxDistanceBetweenPointsCCW( vector<Point> vertices, int start_vertex, int end_vertex);
float findMaxDistanceBetweenPoints( vector<Point> vertices, int start_vertex, int end_index );
int getIndexOfNextVertex( vector<Point> vertices, int start_vertex, int end_vertex );
vector<Point> buildPathBetweenPointsCCW( vector<Point> vertices, int start_vertex, 
        int end_vertex);
float getAngleBetweenPoints( Point a, Point b);
float findDistanceOfPath( vector<Point> vertices );
