#include "node.h"

Node::Node(){
    this->isDistinct = false;
    this->isAlternate = false;
    this->nodeValue = "";
    this->parentNode = NULL;
}

Node::Node( string s ){
    this->isDistinct = false;
    this->isAlternate = false;
    this->nodeValue = s;
    this->parentNode = NULL;
}
/*
Node::~Node(){
    vector<Node*> children = this->childNodes; 
    for( int i=0; i<children.size(); i++ ){
        this->removeChild( i ); 
    }
}*/

void Node::deleteParent(){
   delete this->parentNode; 
   this->parentNode = NULL;
}

/*
void Node::removeChild( int index ){
    vector<Node *> children = this->childNodes;
    if (index < children.size()){
        Node * target = children[ index ];
        delete target; 
        children.erase( children.begin() + index -1 );
    }
}*/
