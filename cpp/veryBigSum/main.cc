#include <iostream>
#include <vector>
#include "src/veryBigSum.cc"

using namespace std;

int main(){
    int numIntegers;
    cout << "Enter the number of values in list" << endl;
    cin >> numIntegers;

    vector<long long int> listOfIntegers( numIntegers );
    cout << "Enter the values" << endl;
    for( int i=0; i<numIntegers; i++ ){
        cin >> listOfIntegers[i];
    }

    cout << calculateVeryLargeSum( listOfIntegers ) << endl;
}
