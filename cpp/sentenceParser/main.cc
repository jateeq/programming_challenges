#include <fstream>
#include <iostream>
#include <sstream>

#include "src/utils.h"


using namespace std;

int main( int argc, char *argv[]){
    //convert the string input to an integer
    stringstream convert_min(argv[1]);
    int min;
    if(!(convert_min >> min)){
       min = 0; 
    }

    stringstream convert_max(argv[2]);
    int max;
    if(!(convert_max >> max)){
       max = 0; 
    }

    string input_filename = argv[3];
    string output_filename = argv[4];

    ifstream input_stream(input_filename);
    ofstream out_stream(output_filename); 

    string line;
    string result;
    while( getline( input_stream, line ) ){
        cout << line << endl; 
        result = get_sentence_or_sequence( line, min, max ); 
        out_stream << result << endl;
    }
    out_stream.close();

    return 0;
}

